import { CandidatosService } from './../services/candidatos.service';
import { Candidatos } from './../models/candidatos';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  candidato = {} as Candidatos;
  candidatos: Candidatos[];

  constructor(private candidatoService: CandidatosService) { }

  ngOnInit() {
    this.getCandidatos();
  }


  // Chama o serviço para obtém todos os carros
  getCandidatos() {
    this.candidatoService.getCandidatos().subscribe((candidatos: Candidatos[]) => {
      this.candidatos = candidatos;
      console.log(candidatos);
    });
  }

}
