# README #

## Lista de Tarefas ##

### 1.	(Prazo: 20min) Crie um arquivo CSS (arquivo teste1.css) para que a pagina HTML (teste1.html) fique com o seguinte aspecto das div’s:

 
### 2.	(Prazo: 20min) Utilizando a biblioteca jQuery realize a listagem dos dados (JSON) retornados pela URL:
 ``` http://69.162.111.190/smscontrol/public/api/list1 ```

#### OBS: A listagem deve ser mostrada na DIV “Content”. A formatação de estilo é de livre escolha, cabendo ao candidato utilizar as técnicas que conhece.

### 3.	(Prazo: 20min) Utilizando o Spring Boot crie um serviço que retorne uma estrutura de dados semelhante ao da questão anterior (JSON). 

OBS: Não é necessário acessar base dados. A estrutura retornada pode ser gerada de forma estática.

### 4.	(Prazo: 20min) Crie uma página utilizando Angular para listar os dados (JSON) retornados pela URL:
 ``` http://69.162.111.190/smscontrol/public/api/list1 ```

#### OBS: Utilizar como template HTML o conteúdo do arquivo teste1.html, realizando as devidas alterações necessárias.
